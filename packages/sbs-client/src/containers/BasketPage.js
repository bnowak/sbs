import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/basketActions';
import Basket from '../components/Basket';

export const BasketPage = (props) => {
  return (
    <Basket
      articles={props.articles}
    />
  );
};

BasketPage.propTypes = {
  actions: PropTypes.object.isRequired,
  articles: PropTypes.array.isRequired,
  basketId: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    articles: state.basket.articles,
    basketId: state.basket.id,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BasketPage);
