import React from 'react';
import {shallow} from 'enzyme';
import {BasketPage} from './BasketPage';
import Basket from '../components/Basket';

describe('<BasketPage />', () => {
  it('should contain <Basket />', () => {
    const actions = {
      getBasket: () => { },
    };
    const articles = [];
    const wrapper = shallow(<BasketPage actions={actions} articles={articles} basketId={123}/>);

    expect(wrapper.find(Basket).length).toEqual(1);
  });
});
