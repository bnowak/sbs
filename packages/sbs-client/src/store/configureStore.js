import {createStore, compose, applyMiddleware} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import swaggerClient from 'redux-swagger-client';// eslint-disable-line
import rootReducer from '../reducers';
import initialState from './../reducers/initialState';

function configureStoreProd(initialState) {
  const middlewares = [
    thunk,
    swaggerClient({url:'http://localhost:8080/api-docs'}),
  ];

  return createStore(rootReducer, initialState, compose(
    applyMiddleware(...middlewares)
    )
  );
}

function configureStoreDev(initialState) {
  const middlewares = [
    reduxImmutableStateInvariant(),
    thunk,
    swaggerClient({url:'http://localhost:8080/api-docs'}),
  ];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  const store = createStore(rootReducer, initialState, composeEnhancers(
    applyMiddleware(...middlewares)
    )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default; // eslint-disable-line global-require
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

const configuredStore = process.env.NODE_ENV === 'production' ? configureStoreProd(initialState) : configureStoreDev(initialState);

export default configuredStore;
