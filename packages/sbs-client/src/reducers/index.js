import { combineReducers } from 'redux';
import basket from './basketReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
  basket,
  routing: routerReducer
});

export default rootReducer;
