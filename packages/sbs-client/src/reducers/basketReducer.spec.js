import reducer from './basketReducer';
import initialState from './initialState';

describe('Reducers::Basket', () => {
  const getInitialState = () => initialState.basket;

  it('should set initial state by default', () => {
    const action = { type: 'unknown' };
    const expected = getInitialState();

    expect(reducer(undefined, action)).toEqual(expected);
  });
});
