import {GET_BASKET_SUCCESS} from '../constants/actionTypes';
import initialState from './initialState';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function basketReducer(state = initialState.basket, action) {
  let newState;
  switch (action.type) {
    case GET_BASKET_SUCCESS:
      newState = Object.assign({}, state);
      newState = Object.assign(newState, JSON.parse(action.payload.data));
      return newState;
    default:
      return state;
  }
}
