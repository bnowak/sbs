import React, {PropTypes} from 'react';
import {getArticleTotal, getBasketTotal} from './../utils/calculator';
import NumberFormatter from './../utils/numberFormatter';

const Basket = ({articles}) => {
  return (
    <ul>
      <li style={{'display': 'flex', 'width': '200px', 'justifyContent': 'space-between'}}>
        <div>Item</div>
        <div>Count</div>
        <div>Value</div>
      </li>
      {
        articles.map((article, idx) => {
          return (
            <li className="article" key={idx} style={{'display': 'flex', 'width': '200px', 'justify-content': 'space-between'}}>
              <div>{article.name}</div>
              <div>{article.count}</div>
              <div>{NumberFormatter.getCurrencyFormattedNumber(getArticleTotal(article))}</div>
            </li>
          );
        })
      }
      <li style={{'display': 'flex', 'width': '200px', 'justifyContent': 'space-between'}}>
        <div>Total</div>
        <div>{NumberFormatter.getCurrencyFormattedNumber(getBasketTotal(articles))}</div>
      </li>
    </ul>
  );
};

Basket.propTypes = {
  articles: PropTypes.array.isRequired
};

export default Basket;
