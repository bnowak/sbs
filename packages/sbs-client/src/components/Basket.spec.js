import React from 'react';
import {shallow} from 'enzyme';
import Basket from './Basket';

describe('<Basket />', () => {
  it('should display basket items', () => {
    const articles = [{
      id: 0,
      name: 'aeiou',
      count: 5,
      price: 0.17
    }];

    const wrapper = shallow(<Basket articles={articles}/>);
    const actual = wrapper.find('.article').length;
    const expected = 1;

    expect(actual).toEqual(expected);
  });

});
