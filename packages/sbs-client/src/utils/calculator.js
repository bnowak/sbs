export const getBasketTotal = (articles) => articles.reduce((total, article) => {
  return total + getArticleTotal(article);
}, 0);

export const getArticleTotal = (article) => {
  return article.count * article.price;
};
