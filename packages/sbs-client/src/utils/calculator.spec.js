import { getBasketTotal, getArticleTotal } from './calculator';

describe('Calculator', () => {
  const articles = [{
    id: 0,
    name: 'aeiou',
    count: 5,
    price: 0.2
  }];
  describe('getBasketTotal', () => {
    it('returns correct sum for basket', () => {
      expect(getBasketTotal(articles)).toEqual(1);
    });
  });
  describe('getArticleTotal', () => {
    it('returns correct sum for article', () => {
      expect(getArticleTotal(articles[0])).toEqual(1);
    });
  });
});
