import * as types from '../constants/actionTypes';

export function getBasket() {
  return function (dispatch) {
    return dispatch({
      type: types.GET_BASKET,
      swagger: (api) => {
        api.default.getBasket(
          {basketId: 1234},
          (basket) => dispatch({
            type: types.GET_BASKET_SUCCESS,
            payload: basket
          }),
          (error) => dispatch({
            type: types.GET_BASKET_FAILED,
            payload: error
          })
        );
      }
    });
  };
}
