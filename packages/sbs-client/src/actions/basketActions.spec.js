import * as ActionCreators from './basketActions';

describe('Actions', () => {
  it('should create an action to get basket', () => {
    const dispatch = jest.fn();
    expect(typeof (ActionCreators.getBasket())).toEqual('function');
    ActionCreators.getBasket()(dispatch);
    expect(dispatch).toBeCalled();
  });
});
