import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import BasketPage from './containers/BasketPage'; // eslint-disable-line import/no-named-as-default
import AboutPage from './components/AboutPage';
import NotFoundPage from './components/NotFoundPage';
import configuredStore from './store/configureStore';
import { getBasket } from './actions/basketActions';

const onEnterAction = (store, dispatchAction) => {
    return (nextState, replace) => {// eslint-disable-line
        store.dispatch(dispatchAction());
    };
};

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="basket" component={BasketPage} onEnter={onEnterAction(configuredStore, getBasket)}/>
    <Route path="about" component={AboutPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);
