'use strict';

exports.getBasket = function(args, res, next) {
  /**
   * Returns basket contents
   *
   * basketId UUID id of the basket to retrieve
   * returns basket
   **/
  var examples = {};
  examples['application/json'] = {
  "id" : 123456789,
  "articles" : [
    {
      "price" : .25,
      "name" : "apple",
      "id" : 0,
      "count": Math.ceil(Math.random()*10)
    },
    {
      "price" : .3,
      "name" : "orange",
      "id" : 1,
      "count": Math.ceil(Math.random()*10)
    },
    {
      "price" : .15,
      "name" : "banana",
      "id" : 2,
      "count": Math.ceil(Math.random()*10)
    },
    {
      "price" : .5,
      "name" : "papaya",
      "id" : 3,
      "promo": "342",
      "count": Math.ceil(Math.random()*10)
    }
  ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

