'use strict';

var url = require('url');

var Default = require('./DefaultService');

module.exports.getBasket = function getBasket (req, res, next) {
  Default.getBasket(req.swagger.params, res, next);
};
