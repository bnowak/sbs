const newman = require('newman');

newman.run({
    collection: require('./sbs.postman_collection.json'),
    reporters: 'cli',
    insecure: true,
  }, process.exit);
