# Sample Basket Service

## Task:
- Develop a simple program that calculates the price of a shopping basket
- Expected duration: ca. 2h

## Requirements:
- Items are presented one at a time, in a list, identified by name - for example "Apple" or “Banana".
- The basket can contain any item multiple times. Items are priced as follows:
- Apples are 25 ct each
- Oranges are 30 ct each
- Bananas are 15 ct each
- Papayas are 50 ct each, but are available as ‘three for the price of two’
- Given a shopping list with items, calculate the total cost of those items.
- The output shall be displayed similar to what you would expect to see on a receipt.

## Running

1. `git clone ...`
2. `npm i`
3. `npm run start`

## Testing
* For all packages or inside each package directory `npm t`

### Implementation notes:
To complete the task I've created 2 npm packages for client and server. I am using [lerna](https://lernajs.io/) to manage multiple packages in a monorepo structure. 

#### Server
I have started by creating a yaml descriptor of the data shape using [OpenAPI (swagger)](http://swagger.io/) spec format.
This has allowed me to generate the server using [swagger-codegen](http://swagger.io/swagger-codegen/). Tests are implemented using [Postman/Newman](https://github.com/postmanlabs/newman). After implementing a mock response, we are done with the server. Time spent - ca. 30 min, quick and dirty :). Features implemented: routing, validation, logging, tests. Features ignored: security, performance management, static code analysis, linting, the fact it's es5

#### Client
I have based my client on the awesome [react-slingshot](https://github.com/coryhouse/react-slingshot) starter pack. 
After removing the example store/reducers/components I've 
- added [redux-swagger-client](https://github.com/noh4ck/redux-swagger-client) for the integration with server. 
- created reducers for 1) fetching the data 2) Calculating total amount 3) handling errors
- created presentational and container components responsible for displaying basket data as well as a relevant route.
- after adding some basic tests for the above, the quick and dirty client example is done. Time spent 1:15.
- Note: `utiils/numberFormatter` and `utiils/mathHelper` came with [react-slingshot](https://github.com/coryhouse/react-slingshot)

### How this differs form the production quality code:
- I'd optimize for production: 
    - minification, cdns, etc
    - time to first byte
    - semantic markup
- I'd use at least Es6 if not Typescript across the board
- I'd spent any time at all on CSS - using Aphrodite or Radium
- I'd add storybook component workflow
- I'd add visual CSS regression testing
- I'd consider going isomorphic
- I'd consider using GraphQL / Falcor
- I'd consider using RxJS
- I'd most probably use Ramda / Reselect for "selectors" and memoization.
- I'd have done accessibility testing
- I'd have used  static code quality analysis (sonar)
- I'd containerise the project for DevOps integration (Docker/Ansible)

